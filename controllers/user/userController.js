const express = require('express');

function index(req, res, next) {
    let user = new Object();
    res.json(user);
}
function list(req, res, next) {
    let users = new Array();
    res.json(users);
}


function create(req, res, next) {
    let name = req.body.name;
    res.render('index', { title: 'Se creó un elemento' });
}

function update(req, res, next) {
    res.render('index', { title: 'Se actualizo un elemento' });
}

function destroy(req, res, next) {
    res.render('index', { title: 'Se destruyo un elemento' });
}


module.exports = {
    index,
    list, 
    create,
    update,
    destroy
}